package com.wyweixin.Mybatis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisApplication {

	public static void main(String[] args) {
		SpringApplication.run(MybatisApplication.class, args);
	}

	/**
	 *  SpringBoot Mybatis 填坑
	 *  1.在MySql 中创建表，需要更改编码，字符集为UTF-8 否则中文插入将会抛出异常UncategorizedSQLException 注意是表不是字段！！
     */
}
