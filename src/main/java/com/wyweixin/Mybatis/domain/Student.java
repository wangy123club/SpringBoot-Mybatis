package com.wyweixin.Mybatis.domain;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * @create by wangy on 2018-01-11 14:28
 * @Author <a href="mailto:wangy@drore.com">wangy@drore.com</a>
 */

public class Student {

    private String id;
    private String sname;
    @Min(1)
    @Max(130)
    private int age;
    private String tid;
    private String createTime;
    private Teacher teacher;

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + id + '\'' +
                ", sname='" + sname + '\'' +
                ", age=" + age +
                ", tid='" + tid + '\'' +
                '}';
    }
}
