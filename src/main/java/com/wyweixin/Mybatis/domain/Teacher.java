package com.wyweixin.Mybatis.domain;

/**
 * @create by wangy on 2018-01-11 14:28
 * @Author <a href="mailto:wangy@drore.com">wangy@drore.com</a>
 */
public class Teacher {
    private String id;
    private String tname;
    private String createTime;

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

}
