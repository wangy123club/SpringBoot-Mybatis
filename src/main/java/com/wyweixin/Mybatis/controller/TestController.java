package com.wyweixin.Mybatis.controller;

import com.wyweixin.Mybatis.domain.Student;
import com.wyweixin.Mybatis.domain.Teacher;
import com.wyweixin.Mybatis.mapper.StudentMapper;
import com.wyweixin.Mybatis.mapper.TeacherMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @create by wangy on 2018-01-17 14:01
 * @Author <a href="mailto:wangy@drore.com">wangy@drore.com</a>
 */
@RequestMapping("/api/class")
@RestController
public class TestController {
    @Autowired
    private TeacherMapper teacherMapper;

    @Autowired
    private StudentMapper studentMapper;

    @RequestMapping("queryAllTeacher")
    public List<Teacher> queryAllTeacher(){
        return teacherMapper.getAll();
    }

    @RequestMapping("addTeacher")
    public Teacher addTeacher(Teacher teacher){
        int i = teacherMapper.addTeacher(teacher);
        return teacher;
    }
    @RequestMapping("deleteTeacher")
    public Integer deleteTeacher(String id){
        int i = teacherMapper.deleteTeacher(id);
        return i;
    }

    @RequestMapping("addStudent")
    public Student addStudent(Student student){
        int i = studentMapper.addStudent(student);
        return student;
    }
    @RequestMapping("findById")
    public Map findById( ){
        Map student = studentMapper.findById("b574e2a08fef6f2d4f47d1fe2ee60325");
        return student;
    }
    @RequestMapping("findStudentByTid")
    public Map findStudentByTid( ){
        Map student = studentMapper.findStudentByTid("b574e2a08fef6f2d4f47d1fe2ee60325");
        return student;
    }

    @RequestMapping("selectAllStudent1")
    public Student selectAllStudent1( ){
        Student student = studentMapper.selectAllStudent1();
        return student;
    }



}
