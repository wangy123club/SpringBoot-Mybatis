/*
 * Copyright (C) 2016 Zhejiang DRORE Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.drore.com.
 * Developer Web Site: http://open.drore.com.
 */
package com.wyweixin.Mybatis.constant;
/**
 * @create by wangy on 2018-01-11 14:30
 * @Author <a href="mailto:wangy@drore.com">wangy@drore.com</a>
 */
public class ConstantEnum {

	public enum resource_name{
		
		Student("student","学生表"),Teachet("teacher","教师表"),User("user","用户表");
		
		private String value;
		private String name;
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		private resource_name(String value, String name) {
			this.value = value;
			this.name = name;
		}
	}
	
	



}
