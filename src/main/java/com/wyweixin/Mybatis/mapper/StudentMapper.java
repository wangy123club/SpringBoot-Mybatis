package com.wyweixin.Mybatis.mapper;

import com.wyweixin.Mybatis.domain.Student;
import com.wyweixin.Mybatis.domain.Teacher;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @create by wangy on 2018-01-18 10:00
 * @Author <a href="mailto:wangy@drore.com">wangy@drore.com</a>
 */
@Mapper
public interface StudentMapper {

    List<Teacher> getAll();

    int addStudent(Student student);

    int deleteStudent(String id);


    Map findById(String id);

    Map findStudentByTid(String id);

    Student selectAllStudent1();

}
