package com.wyweixin.Mybatis.mapper;

import com.wyweixin.Mybatis.domain.Teacher;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface TeacherMapper {
      
    List<Teacher> getAll();

    int addTeacher(Teacher teacher);

    int deleteTeacher(String id);
}